# -*- coding: UTF-8 -*-
"""
Question_5
===========
"""
import json
import math
import logging.config
import assin_pkg.question_5

__author__ = "shahbazalam@gyandata.com"
LOGGER = logging.getLogger(__name__)

if "__main__" == __name__:
    # Load json file for logging
    with open("logging.json", mode="r") as fp:
        LOGGING_CONFIG = json.load(fp=fp)
    logging.config.dictConfig(LOGGING_CONFIG)

    angle = int(input("Enter the angle in degree->"))
    n = int(input("Enter the number of iteration->"))
    angle_rad = int(math.radians(angle))
    assin_pkg.question_5.taylor_sin(angle_rad, n)
