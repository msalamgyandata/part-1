# -*- coding: UTF-8 -*-
"""
Question_4
===========
"""
import json
import logging.config
import assin_pkg.question_4

__author__ = "shahbazalam@gyandata.com"
LOGGER = logging.getLogger(__name__)

if "__main__" == __name__:

    # Load json file for logging
    with open("logging.json", mode="r") as fp:
        LOGGING_CONFIG = json.load(fp=fp)
    logging.config.dictConfig(LOGGING_CONFIG)

    arr = []
    n = int(input("Enter the degree of polynomial->"))
    print("Enter the coefficent in incresing order of degree.")
    for i in range(0, n + 1):
        x = int(input("Enter the coefficent of polynomial->"))
        arr.append(x)
    print("Enter the range(two initial values).")
    x0 = int(input("Enter the lower bound value->"))
    x1 = int(input("Enter the upper bound value->"))
    t = int(input("Enter the number of iteration->"))
    assin_pkg.question_4.newton_raphson(arr, x0, x1, t, n)
