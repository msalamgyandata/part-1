# -*- coding: UTF-8 -*-
"""
Question_1
===========
"""
import json
import logging.config
import assin_pkg.question_1

__author__ = "shahbazalam@gyandata.com"
LOGGER = logging.getLogger(__name__)
if "__main__" == __name__:

    # Load json file for logging
    with open("logging.json", mode="r") as fp:
        LOGGING_CONFIG = json.load(fp=fp)
    logging.config.dictConfig(LOGGING_CONFIG)

    s = input("Enter the string->")
    # check the length of string.
    if len(s) == 0:
        LOGGER.error("Input '%s' does not contain any braces", s)
    else:
        LOGGER.info("The result of balancing braces is %s", assin_pkg.question_1.balancebraces(s))
