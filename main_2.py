# -*- coding: UTF-8 -*-
"""
Question_2
===========
"""
import json
import logging.config
import assin_pkg.question_2

__author__ = "shahbazalam@gyandata.com"
LOGGER = logging.getLogger(__name__)

if __name__ == "__main__":

    # Load json file for logging
    with open("logging.json", mode="r") as fp:
        LOGGING_CONFIG = json.load(fp=fp)
    logging.config.dictConfig(LOGGING_CONFIG)

    arr = []
    n = int(input("Enter the degree of polynomial->"))
    print("Enter the coefficent in decreasing order of degree.")
    for i in range(0, n + 1):
        x = int(input("Enter the coefficent of polynomial->"))
        arr.append(x)
    z = int(input("Enter the value of critical point->"))
    LOGGER.info("Value of polynomial is %s at point %s", assin_pkg.question_2.horner(arr, n, z), z)
