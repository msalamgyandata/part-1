# -*- coding: UTF-8 -*-
"""
Question_3
===========
"""
import json
import logging.config
import assin_pkg.question_3

__author__ = "shahbazalam@gyandata.com"
LOGGER = logging.getLogger(__name__)

if "__main__" == __name__:

    # Load json file for logging
    with open("logging.json", mode="r") as fp:
        LOGGING_CONFIG = json.load(fp=fp)
    logging.config.dictConfig(LOGGING_CONFIG)

    n = int(input("Enter the length of signal->"))
    arr = []
    for i in range(n):
        x = int(input("Enter the value in signal->"))
        arr.append(x)
    t = int(input("Enter the value of lag->"))
    logging.info(
        "Autocorrelation of signal for lag %s is %s",
        t,
        assin_pkg.question_3.auto_correlation(arr, n, t),
    )
