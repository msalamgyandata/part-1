assin\_pkg package
==================

Submodules
----------

assin\_pkg.question\_1 module
-----------------------------

.. automodule:: assin_pkg.question_1
   :members:
   :undoc-members:
   :show-inheritance:

assin\_pkg.question\_2 module
-----------------------------

.. automodule:: assin_pkg.question_2
   :members:
   :undoc-members:
   :show-inheritance:

assin\_pkg.question\_3 module
-----------------------------

.. automodule:: assin_pkg.question_3
   :members:
   :undoc-members:
   :show-inheritance:

assin\_pkg.question\_4 module
-----------------------------

.. automodule:: assin_pkg.question_4
   :members:
   :undoc-members:
   :show-inheritance:

assin\_pkg.question\_5 module
-----------------------------

.. automodule:: assin_pkg.question_5
   :members:
   :undoc-members:
   :show-inheritance:

assin\_pkg.question\_6 module
-----------------------------

.. automodule:: assin_pkg.question_6
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: assin_pkg
   :members:
   :undoc-members:
   :show-inheritance:
