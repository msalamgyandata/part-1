assin\_pkg package
==================

.. automodule:: assin_pkg
   :members:
   :undoc-members:
   :show-inheritance:
   :private-members:

Submodules
----------

.. toctree::
   :maxdepth: 4

   assin_pkg.question_1
   assin_pkg.question_2
   assin_pkg.question_3
   assin_pkg.question_4
   assin_pkg.question_5
   assin_pkg.question_6
