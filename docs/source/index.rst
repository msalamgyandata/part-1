.. sphinx_file documentation master file, created by
   sphinx-quickstart on Mon Feb  7 02:56:25 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sphinx_file's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   assin_pkg.question_1
   assin_pkg.question_2
   assin_pkg.question_3
   assin_pkg.question_4
   assin_pkg.question_5
   assin_pkg.question_6


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
