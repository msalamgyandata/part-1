# -*- coding: UTF-8 -*-
"""
Newton Raphson
================
This module find the root of an equation in a given interval using Newton-Raphson.
"""
import math
import logging
import matplotlib.pyplot as plt

LOGGER = logging.getLogger(__name__)


def math_func(arr, poin, num):
    """
    This module calculates the value of a polynomial at a given point.

    :param arr: The given list of polynomial coefficent
    :type arr: list

    :param poin: critical point.
    :type poin: int

    :param num: polynomial degree.
    :type num: int

    :return: The polynomial's return value at a given point.
    :rtype: float
    """
    res = 0
    for i in range(num + 1):
        res += arr[i] * math.pow(poin, i)
    return res


def derivative_of_math_func(arr, x_0, num):
    """
    This module calculates the derivative of the polynomial at a given point..

    :param arr: The given list of polynomial coefficent
    :type arr: list

    :param x_0: critical point.
    :type x_0: int

    :param num: polynomial degree.
    :type num: int

    :return: return value of the derivative polynomial at a given point.
    :rtype: float
    """
    res = 0
    for i in range(1, num + 1):
        res += arr[i] * i * math.pow(x_0, i - 1)
    return res


def show_plot(epoch, error):
    """
    This module shows the epoch v/s error plot.

    :param epoch: iteration number
    :type epoch: list

    :param error: error value
    :type error: list
    """
    plt.figure(figsize=(10, 5))
    plt.plot(epoch, error, color="red")
    plt.xlabel("Epoch")
    plt.ylabel("Error")
    plt.show()


def newton_raphson(arr, x_0, x_1, temp, num):
    """
    This module calculates the root of an equation in a given interval using Newton-Raphson.

    :param arr: The given list of polynomial coefficent.
    :type arr: list

    :param x_0: interval's lower bound.
    :type x_0: int

    :param x_1: interval's upper bond.
    :type x_1: int

    :param num: polynomial degree.
    :type num: int

    :param temp: iteration number.
    :type temp: int
    """
    epoch = []
    error = []
    # If one value is negative and one value is positive, then it intersects the x axis.
    if (math_func(arr, x_0, num) * math_func(arr, x_1, num)) > 0:
        print(
            "Error, there doesn't exist real roots/more than "
            "one roots in this region [{x_0},{x_1}], enter another region."
        )
    else:
        # find the interval's midpoint..
        x_new: int = (x_0 + x_1) / 2
        for i in range(temp):
            h_0 = math_func(arr, x_new, num) / derivative_of_math_func(arr, x_new, num)
            print(
                "Iteration no. {i + 1}, function value {math_func(arr, x_new, num)}",
                "gradient function value {derivative_of_math_func(arr, x_new, num)}",
                "residual error {abs(x_new - (x_new - h_0)}"
            )
            error.append(abs(x_new - (x_new - h_0)))
            epoch.append(i + 1)
            x_new -= h_0
        print('The value of the root is {x_new} in the range [{x_0},{x_1}]')
        show_plot(epoch, error)
