# -*- coding: UTF-8 -*-
"""
Balance parenthesis
=====================
This module contains the method to check a given string's balance parenthesis.
"""

__author__ = "shahbazalam@gyandata.com"


def removeal_alphabet(str_1):
    """
    Method to remove all the alphabet and numbers from the string

    :param str_1: The given string, to remove all the alphabets and numbers
    :type str_1: str

    :return: string which contains only parenthesis.
    :rtype: str
    """
    str_i = ""
    for i in str_1:
        if i in ["{", "}", "[", "]", "(", ")"]:
            str_i += i
    return str_i


def balancebraces(str_2):
    """
    Method uses this to check if a given string has balanced parenthesis.

    :param str_2: The given string, to check string is balance parenthesis or not.
    :type str_2: str

    :return: return the boolean value.
    :rtype: bool
    """

    # It will return false if the check string contains only a number.
    if str_2.isdigit() is True:
        return False
    # Take the alphabet out of the string.
    str_3 = removeal_alphabet(str_2)
    stack = []
    # Add all the open braces to the stack.
    for char in str_3:
        if char in ["(", "[", "{"]:
            stack.append(char)
        else:
            # If the last element of the stack is the opposite of the
            # current character, return false.
            if not stack:
                return False
            if (char == ")" and stack[-1] != "(") or (char == "]" and stack[-1] != "["):
                return False
            if char == "}" and stack[-1] != "{":
                return False
            stack.pop()
    # If the stack is not empty, return false; else, return true.
    return len(stack) == 0
