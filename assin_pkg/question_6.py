# -*- coding: UTF-8 -*-
"""
Closest power of 2
===================================
This module contains the method to compute the closest power of two.
"""

import logging
import math

__author__ = "shahbazalam@gyandata.com"

LOGGER = logging.getLogger(__name__)


def closest_power(num):
    """
    Method calculates the closest power of 2 with respect to the given number

    :param num: The given number whose closest power needs to be returned
    :type num: int

    :return: Closest power of two
    :rtype: int
    """
    # Compute the log of the given number with base 2
    x_0 = int(math.log(num, 2))

    # Compute the powers of 2 with the closest integer value
    low, high = int(pow(2, x_0)), int(pow(2, x_0 + 1))

    # Returning the lowest power, if the powers of 2 are equidistant
    # Return the lowest power, if lowest power is closer to the given number and equidistant.
    if abs(low - num) <= abs(high - num):
        return low

    # Return the highest power, if highest power is closer to the given number
    else:
        return high
