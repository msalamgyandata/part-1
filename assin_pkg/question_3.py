# -*- coding: UTF-8 -*-
"""
Auto Correlation
===================
The module calculates autocorrelation of a given list.
"""


def cal_mean(arr):
    """
    The module calculates autocorrelation of a given list

    :param arr: The given list whose mean needs to be returned
    :type arr: list

    :return: return mean value.
    :rtype: float
    """
    sum_num = 0
    for val in arr:
        sum_num += val
    return sum_num / len(arr)


def auto_correlation(arr, num, tem):
    """
    Calculate Autocorrelation of given list.

    :param arr: The given list whose autocorrelation needs to be returned.
    :type arr: list

    :param num: iteration number.
    :type num: int

    :param tem: The given lag value
    :type tem: int

    :return: Autocorrelation of list
    :rtype: float
    """
    numre = 0
    denem = 0
    # Calculate mean of list
    mean = cal_mean(arr)
    # Use formula of autocorrelation.
    for i in range(num - tem):
        numre += (arr[i] - mean) * (arr[i + tem] - mean)
    for i in range(num):
        denem += (arr[i] - mean) ** 2
    return numre / denem
