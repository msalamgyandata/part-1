# -*- coding: UTF-8 -*-
"""
Horner function
==================
This module calculates the value of an equation at a given point using Horner's method.
"""


def horner(fun, num, point):
    """
    This module takes polynomial coefficent, iteration number and critical
     point as inputs and return value of polynomial at that point.

    :param fun: The given list of polynomial coefficent.
    :type fun: list

    :param num: iteration number
    :type num: int

    :param point: The given point at which apply horner's method
    :type point: int

    :return: return value of the function at that point.
    :rtype: int
    """
    res = fun[0]
    for i in range(1, num + 1):
        res = res * point + fun[i]
    return res
