# -*- coding: UTF-8 -*-
"""
Taylor series
==================
This module take a point as input gives sin value at that point
as output using taylor series methord.
"""
import math
from decimal import Decimal
import logging
import matplotlib.pyplot as plt

LOGGER = logging.getLogger(__name__)


def show_plot(epoch, error):
    """
    This module shows the epoch v/s error plot.

    :param epoch: iteration number
    :type epoch: list

    :param error: error value
    :type error: list
    """
    plt.figure(figsize=(10, 5))
    plt.plot(epoch, error, color='green')
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.show()


def taylor_sin(x_0, num):
    """
    This module calculates the value of the sin function at a given point using the Taylor series.

    :param x_0: The given point in terms of degree.
    :type x_0: int

    :param num: iteration number
    :type num: int
    """
    out = math.sin(x_0)
    sin_approx = 0
    epoch = []
    error = []
    # Calculate the coefficent, numerator, and denominator for each iteration..
    for i in range(num):
        coef = (-1) ** i
        num = Decimal(x_0 ** (2 * i + 1))
        # If the value is too large, it will give an overflow situation.
        if num > 10 ** 350 - 1:
            LOGGER.error("Overflow situation")
            break
        denom = Decimal(math.factorial(2 * i + 1))
        sin_approx += coef * (float(num) / float(denom))
        print('Iteration %s value approx %s error is %s', i + 1, sin_approx, abs(sin_approx - out))
        epoch.append(i + 1)
        error.append(abs(sin_approx - out))
    show_plot(epoch, error)
