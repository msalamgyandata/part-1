# -*- coding: UTF-8 -*-
"""
Question_6
===========
"""
import json
import logging.config
import assin_pkg.question_6

__author__ = "shahbazalam@gyandata.com"
LOGGER = logging.getLogger(__name__)

if "__main__" == __name__:
    # Load json file for logging
    with open("logging.json", mode="r") as fp:
        LOGGING_CONFIG = json.load(fp=fp)
    logging.config.dictConfig(LOGGING_CONFIG)

    n = int(input("Enter an Integer->"))
    result = assin_pkg.question_6.closest_power(n)
    LOGGER.info("Result is %d", result)
